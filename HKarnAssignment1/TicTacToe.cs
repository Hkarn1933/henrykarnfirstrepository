﻿using HKarnAssignment1.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HKarnAssignment1
{
    public partial class TicTacToe : Form
    {
        public TicTacToe()
        {
            InitializeComponent();
            TicTacToeBoard();
        }

        public bool isTurn = true;
        public bool isWin = false;
        PictureBox[,] pbxBoard = new PictureBox[3, 3];
        public int nClicked = 0;


        //Initializes the "board" of tic tac toe
        private void TicTacToeBoard()
        {
            
            int nX = 12, nY = 12, nSize = 100;
            for (int i = 0; i < pbxBoard.GetLength(0); i++)
            {
                for (int j = 0; j < pbxBoard.GetLength(1); j++)
                {
                    pbxBoard[i,j] = new PictureBox();
                    pbxBoard[i,j].Location = new Point(nX, nY);
                    pbxBoard[i,j].Size = new Size(nSize, nSize);
                    pbxBoard[i,j].BackColor = Color.Gray;
                    pbxBoard[i, j].Click += new EventHandler(pbxTicClicked);
                    pbxBoard[i, j].Image = null;
                    this.Controls.Add(pbxBoard[i,j]);
                    nX += 101;
                }
                nY += 101;
                nX = 12;
            }
        }


        //Sets clicked PictureBox to image based on whose turn it is
        private void pbxTicClicked(object sender, EventArgs e)
        {
            PictureBox p = (PictureBox)sender;
            Bitmap bmX = new Bitmap(Resources.x, new Size(100, 100));
            Bitmap bmO = new Bitmap(Resources.o, new Size(100, 100));

            if (isTurn && p.Image == null)
            {
                p.Image = bmX;
                p.Name = "X";
                isTurn = false;
                nClicked++;
            }
            else if (!isTurn && p.Image == null)
            {
                p.Image = bmO;
                p.Name = "O";
                isTurn = true;
                nClicked++;
            }
            //If winner is decided, will clear the board and start a new game of Tic tac toe
            if (Winner())
            {
                Controls.Clear();
                TicTacToeBoard();
                nClicked = 0;
            }
            

        }
        //Checks if someone has won every turn
        bool Winner()
        {

            Bitmap bmX = new Bitmap(Resources.x, new Size(100, 100));
            int nWinX = 0, nWinY = 0;
            string sWinner = "";
            string sImg = "X";
            for (int k = 0; k < 2; k++)
            {
                for (int i = 0; i < pbxBoard.GetLength(0); i++)
                {
                    for (int j = 0; j < pbxBoard.GetLength(1); j++)
                    {
                        if (pbxBoard[i, j].Name == sImg)
                        {
                            nWinX += 1;
                            sWinner = sImg;
                        }
                        if (pbxBoard[j, i].Name == sImg)
                        {
                            nWinY += 1;
                            sWinner = sImg;
                        }
                    }

                    if ((pbxBoard[0, 0].Name == sImg && pbxBoard[1, 1].Name == sImg && pbxBoard[2, 2].Name == sImg) ||
                        (pbxBoard[0, 2].Name == sImg && pbxBoard[1, 1].Name == sImg && pbxBoard[2, 0].Name == sImg) ||
                        nWinX == 3 || nWinY == 3)
                    {
                        MessageBox.Show($"{sWinner} wins!");
                        isTurn = true;
                        nClicked = 0;
                        return true;
                    }
                    if (nClicked == 9)
                    {
                        MessageBox.Show($"Draw");
                        isTurn = true;
                        nClicked = 0;
                        return true;
                    }
                    //unused code
                    //if (nWinX == 3 || nWinY == 3)
                    //{
                    //    MessageBox.Show($"{sWinner} wins!");
                    //    isTurn = true;
                    //    nClicked = 0;
                    //    return true;
                    //}

                    nWinX = 0;
                    nWinY = 0;
                }
                sImg = "O";
            }
            return false;
        }
    }
}
